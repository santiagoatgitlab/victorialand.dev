<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MY_Controller extends CI_Controller 
 { 
	
	
	public function check_log(){

		if (!isset($this->session->id)){
			header('location: /index.php/login');
		}
		
	}	 
	 
	   //Load layout    
	public function layout($view, $data) {
		 // making temlate and send data to view.
		$this->load->view('head',$data);
		$this->load->view($view,$data);
		$this->load->view('foot',$data);
   	}
}
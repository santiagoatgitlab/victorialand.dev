<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php foreach ($projects as $project) { ?>
<a href="#content">
<div class="w3-card w3-hover-shadow w3-margin vl-width-50">
	<div class="w3-container w3-orange w3-text-white">
		<h4><?php echo $project['name']; ?></h4>
	</div>
	<div class="w3-container">
		<p><?php echo $project['description']; ?></p>
	</div>
</div>
</a>	
<?php } ?>

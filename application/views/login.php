<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Victorialand - Log in</title>
	
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/styles/w3.css">
	<link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.png">
	<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.0.min.js" ></script>
</head>
<body>
	<div class="w3-border w3-center">
		<div class="w3-card w3-margin-top w3-margin-left" style="width:30%">
			<div class="w3-container w3-blue">
				<h3>Log in</h3>
			</div>
			<form id="loginForm" action="" method="post" class="w3-container">
				<?php if ($error) { ?>
				<div class="w3-panel w3-border w3-border-black w3-red w3-padding">
					<label>Usuario o contraseña no válidos</label>
				</div>	
				<?php } ?>
				<p>
					<label>Nombre de usuario</label>
					<input type="text" class="w3-input w3-round-large w3-border-0" name="username" value="<?php echo $username; ?>">
				</p>	
				<p>
					<label>Contraseña</label>
					<input type="password" class="w3-input w3-round-large w3-border-0" name="password">
				</p>	
				<p>
					<input id="submit" type="submit" class="w3-button w3-pink" value=">>">
				</p>
			</form>	
		</div>
	</div>
</body>
</html>
<script>
	$(document).ready(function(){
		$('#loginForm').click(function(){
			$('#login').submit();
		})
	});
</script>
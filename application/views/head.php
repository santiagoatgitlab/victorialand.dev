<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Victorialand</title>
	
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/styles/w3.css">
	<link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.png">
	<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.0.min.js" ></script>
</head>
<body>

<div id="header" class="w3-display-container w3-sand w3-text-pink">
	<a href="/#content"><img src="<?php echo base_url(); ?>assets/img/home.png" style="width:100%"></a>
	<div class="w3-margin-left w3-display-topleft">
		<h1 class="w3-jumbo">Victorialand</h1>
	</div>	
	<div class="w3-display-topright w3-margin-right w3-display-hover w3-animate-opacity">
		<p class="w3-tiny">Bienvenidos a este maravilloso sitio</p>
	</div>
</div>
<div id="content" class="w3-container">	
<div class="w3-bar w3-padding">
	<a href="/index.php/proyectos#content" class="w3-btn w3-hover-blue w3-round w3-border w3-border-teal">Proyectos</a>
	<a href="/index.php/ideas#content" class="w3-btn w3-hover-blue w3-round w3-border w3-border-teal">Ideas</a>
	<a href="/index.php/tareas#content" class="w3-btn w3-hover-blue w3-round w3-border w3-border-teal">Administracion de tareas</a>
	<a class="w3-btn w3-hover-blue w3-round w3-border w3-border-teal w3-disabled">Ganar mucha plata</a>
</div>
	
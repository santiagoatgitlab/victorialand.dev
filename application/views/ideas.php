<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php foreach ($ideas as $idea) { ?>
<a href="#content">
<div class="w3-card w3-margin" style="width:40%; display:block;">
	<div class="w3-container w3-blue">
		<h3><?php echo $idea['title']; ?></h3>
	</div>
	<div class="w3-container">
		<p><?php echo $idea['description']; ?></p>
	</div>
</div>
</a>
<?php } ?>
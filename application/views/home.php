<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<ul class="w3-ul w3-card-4">
	<li class="w3-indigo">
		<h3>Gente</h3>
	</li>	
	<?php foreach ($users as $user) { ?>
	<li class="w3-hover-sand">
		<img src="<?php echo base_url() . "assets/img/" . $user['picture']?>" class="w3-left w3-circle w3-margin-right" style="width:50px">
		<span class="w3-large"><?php echo ucfirst($user['username']); ?></span><br>
		<span class="w3-text-gray"><?php echo $user['position']; ?></span>			
	</li>
	<? } ?>
</ul>
<ul class="w3-ul w3-card-4 w3-margin-top">
	<li class="w3-pink">
		<h3>Tecnologías</h3>
	</li>	
	<li class="w3-hover-sand">
		<span class="w3-large">HTML</span><br>
	</li>
	<li class="w3-hover-sand">
		<span class="w3-large">Git</span><br>
	</li>
	<li class="w3-hover-sand">
		<span class="w3-large">Java</span><br>
	</li>
	<li class="w3-hover-sand">
		<span class="w3-large">Php</span><br>
	</li>
	<li class="w3-hover-sand">
		<span class="w3-large">MySQL</span><br>
	</li>
	<li class="w3-hover-sand">
		<span class="w3-large">GNU / Linux</span><br>
	</li>
	<li class="w3-hover-sand">
		<span class="w3-large">Jquery</span><br>
	</li>
	<li class="w3-hover-sand">
		<span class="w3-large">w3.css</span><br>
	</li>
</ul>
<table class="w3-table-all w3-margin-top">
	<tr>
		<th>Nombre</th>
		<th>Apellido</th>
		<th>Nombre de usuario</th>
	</tr>	
	<?php foreach ($users as $user) { ?>
	<tr class="w3-hover-sand">
		<td><?php echo $user['name']; ?></td>
		<td><?php echo $user['surname']; ?></td>
		<td><?php echo $user['username']; ?></td>
	</tr>	
	<?php } ?>
</table><br>

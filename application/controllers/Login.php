<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model('Home_model','',TRUE);
		$data = array();
		$data['error'] = false;
		$data['username'] = '';
		
		if (isset($this->session->id)){
			header('location: /index.php');
		}
		
		
		if ( !empty($this->input->post()) ){
			
			$user_data = $this->Home_model->check_log_user($this->input->post('username'),$this->input->post('password'));
			
			if (!empty($user_data)){
				
				$this->session->set_userdata($user_data[0]);
				header('location: /index.php');
				
			}
			else{
				$data['error'] = true;
				$data['username'] = $this->input->post('username');
			}
		}
		

		$data['ideas'] = $this->Home_model->get_ideas();
		
		$this->load->view('login',$data);
	}
	
	public function logout(){
		
		$this->session->unset_userdata('id');
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('name');
		
	}
	
}
